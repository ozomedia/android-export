// Copyright 2014 OZOMedia.  All rights reserved.
// www.ozomedia.nl


// Enable opening from Mac Finder or Windows Explorer
#target photoshop

// Perform a "step back" action
function stepHistoryBack()
{
	var desc = new ActionDescriptor();
	var ref = new ActionReference();
	ref.putEnumerated (charIDToTypeID ("HstS"), charIDToTypeID ("Ordn"), charIDToTypeID ("Prvs"));
	desc.putReference(charIDToTypeID("null"), ref);
	executeAction(charIDToTypeID("slct"), desc, DialogModes.NO);
}


// We wrap everything in a function so we can use  return  to exit it
function main()
{
	// Exit the script if it's not run from inside Photoshop
	if (BridgeTalk.appName !== "photoshop")
	{
		alert('This is a Photoshop only script');
		return 'cancel'; // Returning  cancel  makes sure the action is not recorded
	}

	// We need an active document for this script to work
	try
	{
		var doc = app.activeDocument;
	} catch (e) {
		alert('No active document');
		return 'cancel';
	}

	// Get the folder name of the active document
	var baseFolder = Folder(doc.fullName.parent);

	// These are the sizes Android uses. Values are percentages of XHDPI (Retina) resolution
	var sizes = {
		ldpi: '37.5',
		mdpi: '50',
		hdpi: '75',
		xhdpi: '100',
		xxhdpi: '150',
		xxxhdpi: '200'
	};


	// Trim transparent pixels
	doc.trim(TrimType.TRANSPARENT);

	for (var size in sizes)
	{
		// Check if the folder exists and create it if needed
		var folder = Folder(baseFolder + '/' + size);
		if (! folder.exists)
			folder.create();

		var filename = File (baseFolder + '/' + size + '/' + doc.activeLayer.name);

		doc.resizeImage(sizes[size] + ' percent');
		doc.saveAs(filename, PNGSaveOptions, true, Extension.LOWERCASE);

		// Undo resize action
		stepHistoryBack();
	}

	// Undo trim action
	stepHistoryBack();
}
main();